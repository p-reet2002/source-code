**STEPS TO CREATE A PROJECT**
1. Select + in the leftmost global sidebar.

2. Select Project under Create. Note: If you are using the new horizontal navigation, select the Create button and select Project from the dropdown menu.

3. Select the Workspace for the project. 

4. Give the project a Name that is short and easily identifies the work your team will do in the project. The project name populates menus and dashboards.

5. Note or modify the Key field. The key acts as a unique identifier used in the project URL.

6. Add a meaningful description which describes the focus of the project. You can see the project description when viewing a list of projects.

7. Select This is a private project if you only want the project name displayed to members of your workspace. For more information, see the info box below.

8. Click Change avatar to add or change a custom avatar.

9. Click Create project.

**Reasons to choose Apache License**

Here’s why I’ve come to think Apache-2.0 is generally the default preferred license for most projects:
Is permissive (same as MIT, BSD-2)
Is wordier so there is less room for misinterpretation
Does not force hardware vendors to do anything
Contains explicit patent language
Has the Apache Foundation behind it


**HELLLO**
Im gurpreet